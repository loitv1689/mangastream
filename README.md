## 1.Install mysql ##

	sudo apt-get update
	sudo apt-get install mysql-server
	sudo mysql_secure_installation
	sudo mysql_install_db

## 2. Install Mongo
 ##
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
	echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
	sudo apt-get update
	sudo apt-get install -y mongodb-org
## 3. Install python
 ##
	sudo apt-get install build-essential checkinstall
	sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
	wget http://python.org/ftp/python/2.7.10/Python-2.7.10.tgz
	tar -xvf Python-2.7.10.tgz
	cd Python-2.7.10
	./configure
	make
	sudo checkinstall
## 3.1 isntall pip 
 ##
	 wget https://bootstrap.pypa.io/get-pip.py
	sudo python get-pip.py
## 4. install lib pymongo
 ##
	sudo pip install pymongo
## 5. install lib MySQLDB
 ##
	sudo apt-get install libmysqlclient-dev
	sudo pip install MySQL-python
## 6.install scrapy
 ##
	sudo apt-get install python-dev libxml2-dev libxslt1-dev	
	sudo pip install scrapy
## 7 install fronera
 ##
	pip install frontera[sql]
## 8 run crawl ##