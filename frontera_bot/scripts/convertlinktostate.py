from datetime import datetime
from scrapy.http import Request
from frontera.utils.misc import get_crc32, chunks
from frontera.utils.fingerprint import sha1
from frontera.utils.url import parse_domain_from_url_fast
from time import time, sleep
from pymongo import MongoClient

client = MongoClient()
db = client.mangafox
dbmangastream = client.mangastream
n = db.webpage.find({}).count()
divide = 10
distance = n / 10
for x in range(2,10):
    print 'Insert ' + str(distance) + ' document ...'
    for document in db.webpage.find({},{'_id':1}).skip((x - 1) * distance).limit(distance):
        item = {'fingerprint': sha1(document['_id']), 'state': 1}
        dbmangastream.state.update({ 'fingerprint' : item['fingerprint'] }, {'$set' : item }, upsert=True)

