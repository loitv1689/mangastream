data = {
    'hostname' : 'magastream.com',
    'rule':{
        'page1': {
            'identify' : {
                'url_regex' : [],
                'body_regex': [],
                'body_xpath' : []
            },
            'fields' : {
                'field1':{
                    'default' : 'value_default',
                    'xpath' : '/div/a/@href',
                    'type': 'string',
                    'regex_extract' : ['/'],
                    'regex_sub' : [
                        {
                            'pattern': '',
                            'replace' : ''
                        }
                    ],
                    'validate' : '',
                    'cast' : 'int',
                    'plugin' : 'sum'
                },
                'field2':{
                    'xpath': ['/div/a/@href'],
                    'regex_sub': ['/'],
                    'type': 'array'
                }
            },
            'extra_info':{
                'store': 'story',
                'page_name' : '',
                'plugin': '',
            }
        },

    }
}