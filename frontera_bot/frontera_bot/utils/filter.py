from abc import ABCMeta, abstractmethod, abstractproperty
from frontera.utils.url import parse_domain_from_url_fast
from frontera.exceptions import NotConfigured
from frontera_bot.exceptions import BotException
from pymongo import MongoClient
from cachetools import LRUCache


import re
class FilterBase(object):

    @abstractmethod
    def process(self,response):
        raise NotImplementedError

    @abstractmethod
    def getRule(self,link):
        raise NotImplementedError

    @abstractmethod
    def validateRules(self,obj):
        raise NotImplementedError

    @abstractmethod
    def parse(self,response,exps):
        raise NotImplementedError

    @abstractmethod
    def filterRegex(self,obj,patterns):
        raise NotImplementedError

    @abstractmethod
    def checkDataType(self,obj,regs):
        raise NotImplementedError


class MangaResponseFilter(FilterBase):

    def __init__(self,settings):
        # self.manager = manager
        # settings = manager.settings
        self.LOG_LEVEL = settings.get('LOG_LEVEL')
        mongo_uri = settings.get('MONGO_URI')
        mongo_db = settings.get('MONGO_DATABASE_FILTER')
        cache_size = settings.get('FILTER_CACHE_SIZE')
        self.cache = LRUCache(cache_size)
        if mongo_uri is None or mongo_db is None is None:
            raise NotConfigured

        self.client = MongoClient(mongo_uri)
        self.db = self.client[mongo_db]
        self.collection = self.db['filter']
        self.collection.ensure_index("hostname", unique=True, drop_dups=True)



    def process(self, response):
        _, hostname, _, _, _, _ = parse_domain_from_url_fast(response.url)
        obj = self.getRule(hostname)
        if obj is None:
            raise  BotException('Not found rule by hostname: {0} '.format(hostname), 1000)
        fields, extra_info = self.getFieldsParse(obj['rule'],response)
        if fields is None:
            return {},False
        doc = self.parse(fields,response)
        doc['extra_info'] = extra_info
        return doc, True


    def getRule(self,hostname):
        if str(hostname) in self.cache:
            return self.cache[str(hostname)]
        else:
            self.cache[str(hostname)] = self.collection.find_one({'hostname':hostname})
        return self.cache[str(hostname)]

    def filterRegex(self, obj, patterns):
        pass

    def checkDataType(self, obj, regs):
        pass

    def parse(self, fields, response):
        doc = {}
        for key, field in fields.iteritems():
            doc[key] = None
            if field.has_key('default'):
                doc[key] = field['default']
            if field.has_key('xpath'):
                if self.LOG_LEVEL == 'DEBUG':
                    print 'Parse ' + key + ' : ' + field['xpath']
                type = 'string'
                if field.has_key('type'):
                    if field['type'] not in ['string','array']:
                        raise BotException("Type not true", 1111)
                    type = field['type']
                if type == "string":
                    value = response.xpath(field['xpath']).extract_first()
                    if value is not None:
                        doc[key] = value
                    if doc[key] is None:
                        raise BotException('field: \'{0}\' xpath: \'{1}\' parse fail url: {2} '.
                                        format(key, field['xpath'], response.url), 1112, field = key )
                elif type == "array":
                    value = response.xpath(field['xpath']).extract()
                    if value is not None:
                        doc[key] = value
                    if len(doc[key]) <= 0:
                        raise BotException('field: \'{0}\' xpath: \'{1}\' parse fail url: {2} '.
                                        format(key, field['xpath'],response.url), 1112, field = key)
            if field.has_key('regex_extract'):
                for pattern in field['regex_extract']:
                    match = re.search(pattern, doc[key])
                    if match is not None:
                        doc[key] = match.group(1)
            if field.has_key('regex_sub'):
                for obj in field['regex_sub']:
                    doc[key] = re.sub(obj['pattern'], obj['replace'], doc[key])
            if field.has_key('cast'):
                doc[key] = self.cast(doc[key],field['cast'])
        return doc


    def validateRules(self, obj):
        # if obj.has_key('rule') == False:
        #     raise 'Not foud rule in obj'
        # for page_key, page_value in obj['rule'].iteritems():
        #     # validate identify
        #     identify = page_value['identify']
        #
        #     # validate fields
        #     fields = page_value['fields']
        pass
    def isMatchXpath(self,xpaths,response):
        for xpath in xpaths:
            if len(response.xpath(xpath)) == 0:
                return False
        return True

    def isMatchRegex(self,regexs,str):
        for pattern in regexs:
            if bool(re.search(pattern, str)) == False:
                return False
        return True

    def getFieldsParse(self,pages,response):
        if len(pages) <= 0:
            raise BotException("Not have rule", 1113)
        # if len(rules) == 1:
        #     page_rule = rules.items()[0]
        #     return page_rule['fields'], page_rule['store']
        # else:
        extra_info = {}
        results = []
        for key, page in pages.iteritems():
            if page.has_key('identify'):
                identify = page['identify']
                if identify.has_key('url_regex'):
                    regexs = identify['url_regex']
                    if self.isMatchRegex(regexs,response.url) == False:
                        continue
                if identify.has_key('body_regex'):
                    regexs = identify['url_regex']
                    if self.isMatchRegex(regexs,response.body) == False:
                        continue
                if identify.has_key('body_xpath'):
                    xpaths = identify['body_xpath']
                    if self.isMatchXpath(xpaths, response) == False:
                        continue
            results.append(page['fields'])
            if page.has_key('extra_info'):
                extra_info = page['extra_info']
        if len(results) > 1:
            raise BotException("Xpath not precise, > 1 page match", 1114)
        if  len(results) == 0:
            #log here
            print 'this page not rule'
            return None, None
        return results[0], extra_info
    def cast(self, value, type):
        if type in ('int','float','string'):
            if type == 'int':
                return int(value)
            elif type == 'float':
                return float(value)
            elif type == 'string':
                return str(value)
            else:
                return value