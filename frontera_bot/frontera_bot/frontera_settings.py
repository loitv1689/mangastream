BOT_NAME = 'frontera_bot'
BACKEND = 'frontera_bot.backends.DFS'
MONGODBBACKEND_ENGINE = 'mongodb://45.76.155.69:27017'
MONGO_DATABASE = 'mangafox'
MAX_REQUESTS = 200000000
MAX_NEXT_REQUESTS = 60
from datetime import timedelta
SQLALCHEMYBACKEND_REVISIT_INTERVAL = timedelta(days=30)
DELAY_ON_EMPTY = 20.0
STORE_CONTENT = True
SQLALCHEMYBACKEND_CLEAR_CONTENT = False
SQLALCHEMYBACKEND_DROP_ALL_TABLES = False

HTTPCACHE_ENABLED = True   # Turns off disk cache, which has low hit ratio during broad crawls
REDIRECT_ENABLED = True
COOKIES_ENABLED = False
DOWNLOAD_TIMEOUT = 120
# RETRY_ENABLED = False   # Retries can be handled by Frontera itself, depending on crawling strategy
DOWNLOAD_MAXSIZE = 10 * 1024 * 1024  # Maximum document size, causes OOM kills if not set
LOGSTATS_INTERVAL = 10  # Print stats every 10 secs to console

# auto throttling
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_DEBUG = False
AUTOTHROTTLE_MAX_DELAY = 5
AUTOTHROTTLE_START_DELAY = 0.25     # Any small enough value, it will be adjusted during operation by averaging
                                    # with response latencies.
RANDOMIZE_DOWNLOAD_DELAY = False

# concurrency
CONCURRENT_REQUESTS = 5           # Depends on many factors, and should be determined experimentally
CONCURRENT_REQUESTS_PER_DOMAIN = 5
# DOWNLOAD_DELAY = 0.0
