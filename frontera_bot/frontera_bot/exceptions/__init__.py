class BotException(Exception):
    def __init__(self, message, code, **kwargs):
        self._code = code
        self.message = message
        self._extra = kwargs

    @property
    def code(self):
        return self._code

    @property
    def extra(self):
        return self._extra