# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from time import time, sleep
from pymongo.errors import BulkWriteError
from cachetools import LRUCache
from frontera.contrib.backends.partitioners import Crc32NamePartitioner
from frontera.contrib.backends.memory import MemoryStates
from frontera.contrib.backends.sqlalchemy.models import DeclarativeBase
from frontera.core.components import Metadata as BaseMetadata, Queue as BaseQueue
from frontera.core.models import Request, Response
from frontera.utils.misc import get_crc32, chunks
from frontera.utils.url import parse_domain_from_url_fast
from pymongo import  DESCENDING, ASCENDING


class Metadata(BaseMetadata):
    def __init__(self, collection, cache_size):
        # self.session = session_cls(expire_on_commit=False)   # FIXME: Should be explicitly mentioned in docs
        # self.model = model_cls
        self.collection = collection
        self.table = DeclarativeBase.metadata.tables['metadata']
        self.cache = LRUCache(cache_size)
        self.logger = logging.getLogger("mongodb.metadata")

    def frontier_stop(self):
        pass
        # self.session.close()

    def add_seeds(self, seeds):
        for seed in seeds:
            o = self._create_page(seed)
            self.cache[o['fingerprint']] = self._merge(o)

    def request_error(self, page, error):
        m = self._modify_page(page) if page.meta['fingerprint'] in self.cache else self._create_page(page)
        m['error'] = error
        self.cache[m['fingerprint']] = self._merge(m)

    def page_crawled(self, response, links):
        r = self._modify_page(response) if response.meta['fingerprint'] in self.cache else self._create_page(response)
        self.cache[r['fingerprint']] = self._merge(r)
        for link in links:
            if link.meta['fingerprint'] not in self.cache:
                self.cache[link.meta['fingerprint']] = self._merge(self._create_page(link))

    def _modify_page(self, obj):
        db_page = self.cache[obj.meta['fingerprint']]
        db_page['fetched_at'] = datetime.utcnow()
        if isinstance(obj, Response):
            db_page['headers'] = obj.request.headers
            db_page['method'] = obj.request.method
            db_page['cookies'] = obj.request.cookies
            db_page['status_code'] = obj.status_code
        return db_page

    def _create_page(self, obj):
        db_page = {}
        db_page['fingerprint'] = obj.meta['fingerprint']
        db_page['url'] = obj.url
        db_page['created_at'] = datetime.utcnow()
        db_page['meta'] = obj.meta
        db_page['depth'] = 0

        if isinstance(obj, Request):
            db_page['headers'] = obj.headers
            db_page['method'] = obj.method
            db_page['cookies'] = obj.cookies
        elif isinstance(obj, Response):
            db_page['headers'] = obj.request.headers
            db_page['method'] = obj.request.method
            db_page['cookies'] = obj.request.cookies
            db_page['status_code'] = obj.status_code
        return db_page

    def update_score(self, batch):
        for fprint, score, request, schedule in batch:
            m ={
                'fingerprint' : fprint,
                'score' : score            
            }
            self._merge(m)
    def _merge(self,obj):
        return self.collection.find_and_modify({ 'fingerprint' : obj['fingerprint'] }, {'$set' : obj }, upsert=True,new=True)
class States(MemoryStates):

    def __init__(self, collection, cache_size_limit):
        super(States, self).__init__(cache_size_limit)
        # self.session = session_cls()
        # self.model = model_cls
        # self.table = DeclarativeBase.metadata.tables['states']
        self.collection = collection
        self.logger = logging.getLogger("mongodb.states")

    def frontier_stop(self):
        self.flush()
        # self.session.close()

    def fetch(self, fingerprints):
        to_fetch = [f for f in fingerprints if f not in self._cache]
        self.logger.debug("cache size %s", len(self._cache))
        self.logger.debug("to fetch %d from %d", len(to_fetch), len(fingerprints))

        for chunk in chunks(to_fetch, 128):
            for state in self.collection.find({ 'fingerprint' : { "$in" : chunk} }):
                self._cache[str(state['fingerprint'])] = state['state']

    def flush(self, force_clear=False):
        for fingerprint, state_val in self._cache.iteritems():
            state = {
                'fingerprint' : fingerprint,
                'state' : state_val
            }
            self._merge(state)
        self.logger.debug("State cache has been flushed.")
        super(States, self).flush(force_clear)
    def _merge(self,obj):
        return self.collection.find_and_modify({ 'fingerprint' : obj['fingerprint'] }, {'$set' : obj }, upsert=True,new=True)

class Queue(BaseQueue):
    def __init__(self, collection, partitions, ordering='default'):
        self.collection = collection
        self.logger = logging.getLogger("sqlalchemy.queue")
        self.partitions = [i for i in range(0, partitions)]
        self.partitioner = Crc32NamePartitioner(self.partitions)
        self.ordering = ordering

    def frontier_stop(self):
        pass
        # self.session.close()

    def _order_by(self, query):
        if self.ordering == 'created':
            return query.sort('created_at',DESCENDING)
        if self.ordering == 'created_desc':
            return query.sort('created_at',DESCENDING)
        if self.ordering == 'updated':
            return query.sort('created_at',ASCENDING)
        return query.sort('score',DESCENDING).sort('created_at',DESCENDING)  # TODO: remove second parameter,
        # it's not necessary for proper crawling, but needed for tests

    def get_next_requests(self, max_n_requests, partition_id, **kwargs):
        """
        Dequeues new batch of requests for crawling.

        :param max_n_requests: maximum number of requests to return
        :param partition_id: partition id
        :return: list of :class:`Request <frontera.core.models.Request>` objects.
        """
        results = []
        try:
            for item in self._order_by(self.collection.find({'partition_id': partition_id}).limit(max_n_requests)):
                method = 'GET' if not item['method'] else str(item['method'])
                r = Request(item['url'], method=method, meta=item['meta'], headers=item['headers'], cookies=item['cookies'])
                r.meta['fingerprint'] = str(item['fingerprint'])
                r.meta['score'] = item['score']
                results.append(r)
                self.collection.delete_one({'fingerprint':item['fingerprint']})
        except Exception, exc:
            self.logger.exception(exc)
        return results

    def schedule(self, batch):
        to_save = []
        for fprint, score, request, schedule in batch:
            if schedule:
                _, hostname, _, _, _, _ = parse_domain_from_url_fast(request.url)
                if not hostname:
                    self.logger.error("Can't get hostname for URL %s, fingerprint %s" % (request.url, fprint))
                    partition_id = self.partitions[0]
                    host_crc32 = 0
                else:
                    partition_id = self.partitioner.partition(hostname, self.partitions)
                    host_crc32 = get_crc32(hostname)
                q = {
                        'fingerprint': fprint,
                        'score' : score,
                        'url' : request.url, 
                        'meta' : request.meta,
                        'headers' : request.headers, 
                        'cookies' : request.cookies, 
                        'method' : request.method,
                        'partition_id' : partition_id, 
                        'host_crc32' : host_crc32, 
                        'created_at' : time()*1E+6
                    }
                to_save.append(q)
                request.meta['state'] = States.QUEUED
        if len(to_save) > 0:
            # try:
            self.collection.insert_many(to_save)
            # except BulkWriteError as bwe:
            #     print(bwe.details)
            #     raise

        

    def count(self):
        return self.collection.find().count()

    def _merge(self,obj):
        return self.collection.find_and_modify({ 'fingerprint' : obj['fingerprint'] }, {'$set' : obj }, upsert=True,new=True)

