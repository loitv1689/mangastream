from __future__ import absolute_import

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.reflection import Inspector

from frontera.core.components import DistributedBackend
from frontera.contrib.backends import CommonBackend
# from frontera.contrib.backends.sqlalchemy.components import Metadata, Queue, States
from frontera_bot.backends.components import Metadata, Queue, States
from frontera.contrib.backends.sqlalchemy.models import DeclarativeBase
from frontera.utils.misc import load_object
from frontera.exceptions import NotConfigured
from pymongo import MongoClient, DESCENDING


class MongodbCustomizeBackend(CommonBackend):
    def __init__(self, manager):
        self.manager = manager
        settings = manager.settings
        drop_all_tables = settings.get('SQLALCHEMYBACKEND_DROP_ALL_TABLES')
        clear_content = settings.get('SQLALCHEMYBACKEND_CLEAR_CONTENT')

        # mongo_hostname = settings.get('MONGO_HOST')
        # mongo_port = settings.get('MONGO_PORT')
        mongo_uri = settings.get('MONGODBBACKEND_ENGINE')
        mongo_db = settings.get('MONGO_DATABASE')
        if mongo_uri is None or mongo_db is None is None:
            raise NotConfigured

        self.client = MongoClient(mongo_uri)
        self.db = self.client[mongo_db]
        if clear_content:
            self.db['metadata'].remove({})
            self.db['queue'].remove({})
            self.db['state'].remove({})
        if drop_all_tables:
            self.db['metadata'].drop()
            self.db['queue'].drop()
            self.db['state'].drop()

        self.db['metadata'].ensure_index("fingerprint", unique=True, drop_dups=True)
        self.db['queue'].ensure_index("fingerprint", unique=True, drop_dups=True)
        self.db['queue'].ensure_index("score")
        self.db['queue'].ensure_index("partition_id")
        self.db['queue'].ensure_index("create_at")
        self.db['state'].ensure_index("fingerprint", unique=True, drop_dups=True)
        self._metadata = Metadata(self.db['metadata'], settings.get('SQLALCHEMYBACKEND_CACHE_SIZE'))
        self._states = States(self.db['state'], settings.get('STATE_CACHE_SIZE_LIMIT'))
        self._queue = self._create_queue(settings)


    def frontier_stop(self):
        super(MongodbCustomizeBackend, self).frontier_stop()
        # self.engine.dispose()

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    @property
    def queue(self):
        return self._queue

    @property
    def metadata(self):
        return self._metadata

    @property
    def states(self):
        return self._states


class FIFOBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy FIFO Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='created')


class LIFOBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy LIFO Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='created_desc')

class UpdateBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy Update Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='updated')

class DFSBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy DFS Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    def _get_score(self, obj):
        return -obj.meta['depth']


class BFSBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy BFS Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    def _get_score(self, obj):
        return obj.meta['depth']


BASE = CommonBackend
LIFO = LIFOBackend
FIFO = FIFOBackend
DFS = DFSBackend
BFS = BFSBackend
UPDATE = UpdateBackend

class Distributed(DistributedBackend):
    def __init__(self, manager):
        self.manager = manager
        settings = manager.settings
        mongo_uri = settings.get('MONGODBBACKEND_ENGINE')
        mongo_db = settings.get('MONGO_DATABASE')
        if mongo_uri is None or mongo_db is None is None:
            raise NotConfigured

        self.client = MongoClient(mongo_uri)
        self.db = self.client[mongo_db]
        self._metadata = None
        self._queue = None
        self._states = None

    @classmethod
    def strategy_worker(cls, manager):
        b = cls(manager)
        settings = manager.settings
        drop_all_tables = settings.get('SQLALCHEMYBACKEND_DROP_ALL_TABLES')
        clear_content = settings.get('SQLALCHEMYBACKEND_CLEAR_CONTENT')
        if clear_content:
            b.db['state'].remove({})
        if drop_all_tables:
            b.db['state'].drop()
        b.db['state'].ensure_index("fingerprint", unique=True, drop_dups=True)
        b._states = States(b.db['state'], settings.get('STATE_CACHE_SIZE_LIMIT'))
        return b

    @classmethod
    def db_worker(cls, manager):
        b = cls(manager)
        settings = manager.settings
        drop = settings.get('SQLALCHEMYBACKEND_DROP_ALL_TABLES')
        clear_content = settings.get('SQLALCHEMYBACKEND_CLEAR_CONTENT')
        if clear_content:
            b.db['metadata'].remove({})
            b.db['queue'].remove({})
        if drop:
            b.db['metadata'].drop()
            b.db['queue'].drop()
        b.db['metadata'].ensure_index("fingerprint", unique=True, drop_dups=True)
        # b.db['queue'].ensure_index("fingerprint", unique=True, drop_dups=True)
        b.db['queue'].ensure_index("score")
        b.db['queue'].ensure_index("partition_id")
        b.db['queue'].ensure_index("create_at")
        b._metadata = Metadata(b.db['metadata'], settings.get('SQLALCHEMYBACKEND_CACHE_SIZE'))
        b._queue = Queue(b.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))
        return b

    @property
    def queue(self):
        return self._queue

    @property
    def metadata(self):
        return self._metadata

    @property
    def states(self):
        return self._states

    def frontier_start(self):
        for component in [self.metadata, self.queue, self.states]:
            if component:
                component.frontier_start()

    def frontier_stop(self):
        for component in [self.metadata, self.queue, self.states]:
            if component:
                component.frontier_stop()

    def add_seeds(self, seeds):
        self.metadata.add_seeds(seeds)

    def get_next_requests(self, max_next_requests, **kwargs):
        partitions = kwargs.pop('partitions', [0])  # TODO: Collect from all known partitions
        batch = []
        for partition_id in partitions:
            batch.extend(self.queue.get_next_requests(max_next_requests, partition_id, **kwargs))
        return batch

    def page_crawled(self, response, links):
        self.metadata.page_crawled(response, links)

    def request_error(self, request, error):
        self.metadata.request_error(request, error)

    def finished(self):
        return NotImplementedError

