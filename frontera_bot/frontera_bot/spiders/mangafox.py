
from spider_base import Bot
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
import re
class Mangafox(Bot):

    name = "mangafox"
    allowed_domains = ["mangafox.me"]
    allow_rules = ['mangafox\.me/manga/[^/]*/$', 'mangafox\.me/manga/.*/1\.html$',
                   'm.mangafox\.me/roll_manga/.*/1\.html$', 'mangafox\.me.*/releases/']
    deny_rules = ['[a-zA-Z0-9].mangafox.me']
    link_extractor = LinkExtractor(
        allow=(allow_rules),
        deny=(deny_rules),
        restrict_xpaths=()
    )

    def extract_links(self, response):
        requests = []
        for link in self.link_extractor.extract_links(response):
            if link.url.find('1.html') > -1:
                link.url = link.url.replace('mangafox.me/manga','m.mangafox.me/roll_manga')
            request = Request(url=link.url)
            request.meta.update(link_text=link.text)
            requests.append(request)
        if response.url.find('m.mangafox.me') > -1 and response.url.find('/1.html') > -1 and not self.containXpath(response,'//*[@id="viewer"]'):
            link = response.url.replace('m.mangafox.me/roll_manga', 'mangafox.me/manga')
            request = Request(url=link)
            request.meta.update(link_text=link)
            requests.append(request)
        if response.url.find('mangafox.me') > -1 and response.url.find('/1.html') > -1 and self.containXpath(response,'//*[@id="image"]/@src'):
            page = response.xpath('//*[@id="top_bar"]/div/div/text()[2]').extract()
            matchOb = re.search('([0-9]+)', page[0])
            if matchOb is not None:
                page = matchOb.group(1)
            link = response.url
            for x in range(2,int(page)+1):
                url = link.replace('1.html',str(x)+'.html')
                request = Request(url=url)
                request.meta.update(link_text=url)
                requests.append(request)
        return requests

    def containXpath(self, httpResponse, xpath):
        try:
            count = len(httpResponse.xpath(xpath))
            if count > 0:
                return True
            else:
                return False
        except Exception, e:
            # print e
            return False