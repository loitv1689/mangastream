
from spider_base import Bot
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
import re
class CoderDeptrai(Bot):

    name = "coderdeptrai"
    allowed_domains = ["coderdeptrai.com"]
    allow_rules = ['http://coderdeptrai.com/']
    link_extractor = LinkExtractor(
        allow=(allow_rules),
        deny=(),
        restrict_xpaths=()
    )
    custom_settings = {
        'frontera_bot.pipelines.mongo_pipeline.MongoDBPipeline': None,
    }