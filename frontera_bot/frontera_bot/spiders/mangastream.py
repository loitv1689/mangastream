import scrapy
from scrapy.http import Request
from scrapy.spiders import Spider
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from datetime import datetime
from frontera_bot.utils.filter import MangaResponseFilter
from frontera_bot.items import Page
from frontera_bot.items import Error
from frontera import Settings
from scrapy.utils.project import get_project_settings
from spider_base import Bot

import sys, traceback
class UpdatemangafoxSpider(Bot):

    def __init__(self):
        settings = get_project_settings()
        self.filter = MangaResponseFilter(settings)
    name = "mangastream"
    allowed_domains = ["mangastream.com","www.24h.com.vn",'mangafox.me']
    link_extractor = LinkExtractor(
        allow=(['http://mangastream.com','http://www.24h.com.vn/bong-da','http://mangafox.me/']),
        deny=(['http://mangastream.com/r']),
        restrict_xpaths=()
    )

