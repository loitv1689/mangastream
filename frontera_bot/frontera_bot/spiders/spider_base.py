import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from datetime import datetime
from frontera_bot.utils.filter import MangaResponseFilter
from frontera_bot.exceptions import BotException
from frontera_bot.items import Page
from frontera_bot.items import Error
from scrapy.utils.project import get_project_settings
import sys, traceback


class Bot(scrapy.Spider):
    def __init__(self):
        settings = get_project_settings()
        self.filter = MangaResponseFilter(settings)

    name = "spiderbase"
    allowed_domains = []
    link_extractor = LinkExtractor(
        allow=([]),
        deny=([]),
        restrict_xpaths=()
    )

    def parse(self, response):
        print 'INFO FetchUrl - ' + response.url + ' - Status: ' + str(response.status)
        self.logger.info('FetchUrl - ' + response.url + ' - Status: ' + str(response.status))
        requests = self.extract_links(response)
        for request in requests:
            yield request
        if response.status == 200:
            try:
                doc, result = self.filter.process(response)
                if result == True:
                    page = Page()
                    page['fingerprint'] = response.meta['frontier_request'].meta['fingerprint']
                    page['url'] = response.url
                    page['content'] = doc
                    yield page
            except BotException, ex:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                ex_detail = traceback.format_tb(exc_traceback)
                yield self._create_error(ex, ex_detail, response)

    def _create_error(self, ex, ex_detail, response):
        e = Error()
        e['url'] = response.url
        e['error'] = ex.message
        e['code'] = str(ex.code)
        e['extra'] = ex.extra
        e['detail'] = ex_detail
        e['response'] = response.body
        e['extra_info'] = {'store': 'error'}
        e['created_at'] = datetime.utcnow()
        e['updated_at'] = datetime.utcnow()
        return e

    def extract_links(self, response):
        requests = []
        for link in self.link_extractor.extract_links(response):
            request = Request(url=link.url)
            request.meta.update(link_text=link.text)
            requests.append(request)
        return requests
