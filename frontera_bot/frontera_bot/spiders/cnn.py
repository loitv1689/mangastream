# -*- coding: utf-8 -*-
import scrapy
import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from frontera_bot.items import WebPage
from datetime import datetime
allow_rules = ["edition.cnn.com"]
class CnnSpider(scrapy.Spider):
    name = "cnn"
    allowed_domains = ["edition.cnn.com"]
    link_extractor = LinkExtractor(
        allow=(allow_rules),
        deny=(),
        restrict_xpaths=()
    )

    def parse(self, response):
        print 'INFO FetchUrl - ' + response.url + ' - Status: ' + str(response.status)
        self.logger.info('FetchUrl - ' + response.url + ' - Status: ' + str(response.status))
        for link in self.link_extractor.extract_links(response):
            request = Request(url=link.url)
            request.meta.update(link_text=link.text)
            yield request
        if response.status == 200:
            webpage = WebPage()
            webpage['_id'] = response.url
            webpage['url'] = response.url
            webpage['status_code'] = response.status
            webpage['is_fetch'] = 1
            webpage['response'] = response
            webpage['content'] = response.body
            webpage['create_at'] = datetime.utcnow()
            webpage['update_at'] = datetime.utcnow()
            yield webpage
