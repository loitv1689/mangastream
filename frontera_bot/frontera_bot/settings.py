BOT_NAME = 'frontera_bot'

SPIDER_MODULES = ['frontera_bot.spiders']
NEWSPIDER_MODULE = 'frontera_bot.spiders'
MONGO_URI = 'mongodb://45.76.155.69:27017'
MONGO_DATABASE = 'mangafox'
MONGO_DATABASE_FILTER = 'filter'
LOG_ENABLED = True
LOG_LEVEL = 'ERROR'
FILTER_CACHE_SIZE = 100
# CLOSESPIDER_TIMEOUT = 30 * 60
ITEM_PIPELINES = {
   'frontera_bot.pipelines.mongo_pipeline.MongoDBPipeline': 300,
}
DOWNLOAD_TIMEOUT=10
RETRY_ENABLED = True
# RETRY_TIMES =10
# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408]
# DEPTH_PRIORITY = 100
SPIDER_MIDDLEWARES = {}
DOWNLOADER_MIDDLEWARES = {}
SPIDER_MIDDLEWARES.update({
    'frontera.contrib.scrapy.middlewares.seeds.file.FileSeedLoader': 998
})
SPIDER_MIDDLEWARES.update({
    'frontera.contrib.scrapy.middlewares.schedulers.SchedulerSpiderMiddleware': 999,
})

DOWNLOADER_MIDDLEWARES.update({
    # 'frontera_bot.middlewares.FilterLinks':89,
    # 'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': 90,
    # 'frontera_bot.middlewares.RandomProxy':100,
    # 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    # 'frontera_bot.middlewares.RandomUserAgentMiddleware': 400,
    'scrapy.spidermiddlewares.offsite.OffsiteMiddleware' : 624,
    'frontera.contrib.scrapy.middlewares.schedulers.SchedulerDownloaderMiddleware': 999,
})

SCHEDULER = 'frontera.contrib.scrapy.schedulers.frontier.FronteraScheduler'
FRONTERA_SETTINGS = BOT_NAME + '.frontera_settings'
SEEDS_SOURCE = 'seed.txt'

PROXY_LIST = 'proxylist.txt'
