# -*- coding: utf-8 -*-
from frontera.settings.default_settings import MIDDLEWARES
MAX_REQUESTS = 2000000
MAX_NEXT_REQUESTS = 256
DELAY_ON_EMPTY = 1
CONCURRENT_REQUESTS = 10           # Depends on many factors, and should be determined experimentally
CONCURRENT_REQUESTS_PER_DOMAIN = 10
MIDDLEWARES.extend([
    'frontera.contrib.middlewares.domain.DomainMiddleware',
    'frontera.contrib.middlewares.fingerprint.DomainFingerprintMiddleware'
])

#--------------------------------------------------------
# Crawl frontier backend
#--------------------------------------------------------
BACKEND = 'frontera.contrib.backends.remote.messagebus.MessageBusBackend'
SPIDER_FEED_PARTITIONS = 10

