from frontera_bot.plugins import PluginBase
from pymongo import MongoClient
import re
from datetime import datetime
class Plugin(PluginBase):

    def __init__(self,settings):
        mongo_uri = settings.get('MONGO_URI'),
        mongo_db = settings.get('MONGO_DATABASE')
        client = MongoClient(mongo_uri)
        self.db = client[mongo_db]
        self.db['chapter'].ensure_index("chap_url_id", drop_dups=True)
        self.db['chapter'].ensure_index("story_url")


    def filter(self, item):
        is_has_story = False
        chap_url_id = re.sub('/[\d]+.html','',item['url'])
        story = self.db.story.find_one({'source': item['story_url']})
        chapter = None
        if story is not None:
            is_has_story = True
        chapter = self.db.chapter.find_one({'chap_url_id': chap_url_id}, {'_id': 1})

        if chapter is None:
            chapter = {}
            if is_has_story:
                chapter['story_id'] = story['_id']
            chapter['story_url'] = item['story_url']

            matchOb = re.search('/v([0-9a-zA-Z]*)/', item['url'])
            if matchOb is not None:
                chapter['vol'] = matchOb.group(1)
            matchOb = re.search('/c([0-9\.]*)/', item['url'])
            if matchOb is not None:
                chapter['chap'] = float(matchOb.group(1))

            chapter['url'] = item['url']
            chapter['chap_url_id'] = chap_url_id
            chapter['updated_at'] = datetime.utcnow()
            chapter['created_at'] = datetime.utcnow()
            chapter['content'] = {}
            is_has_notifi = False
            if item['extra_info']['id'] == 'schapter':
                chapter['content'][str(item['page'])] = item['content']
                if str(item['page']) == '1':
                    is_has_notifi = True
            elif item['extra_info']['id'] == 'mchapter':
                chapter['content'] = item['content']
                is_has_notifi = True
            result = self.db.chapter.insert_one(chapter)
            if is_has_notifi and is_has_story :
                for user in self.db.user.find({'favorite': story['_id'], 'is_push_notifi': 1}):
                    self.db.user_notification.insert_one({
                        'user_id': user['_id'],
                        'story_id': story['_id'],
                        'chapter_id': result.inserted_id,
                        'story': story['name'],
                        'chapter': chapter['name'],
                        'is_read': 0,
                        'is_push': 0,
                        'updated_at': datetime.utcnow(),
                        'created_at': datetime.utcnow()
                    })
            if is_has_story:
                lastest_chap = {
                    'id': result.inserted_id,
                    'name': chapter['name'],
                    'chap': chapter['chap'],
                }
                if chapter.has_key('vol'):
                    lastest_chap['vol'] = chapter['vol']
                self.db.story.update_one({'_id': story['_id']}, {'$set': {
                    'update_at': datetime.utcnow(),
                    'lastest_chap': lastest_chap
                }})
        else:
            if item['extra_info']['id'] == 'schapter':
                self.db.chapter.update_one({'_id': chapter['_id']}, {'$set': {'content.' + item['page']: item['content']}})
            elif item['extra_info']['id'] == 'mchapter':
                self.db.chapter.update_one({'_id': chapter['_id']}, {'$set': {'content': item['content']}})
            return True