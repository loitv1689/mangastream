from abc import  abstractmethod

class PluginBase(object):

    @abstractmethod
    def filter(self,doc):
        pass

