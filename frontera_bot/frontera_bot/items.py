import scrapy

class WebPage(scrapy.Item):
    _id = scrapy.Field()
    url = scrapy.Field()
    domain = scrapy.Field()
    content = scrapy.Field()
    status_code = scrapy.Field()
    is_fetch = scrapy.Field()
    response = scrapy.Field()
    is_update = scrapy.Field()
    create_at = scrapy.Field()
    update_at = scrapy.Field()
    pass
class Page(scrapy.Item):
    fingerprint = scrapy.Field()
    url = scrapy.Field()
    content = scrapy.Field()
class Error(scrapy.Item):
    url = scrapy.Field()
    error = scrapy.Field()
    code = scrapy.Field()
    extra = scrapy.Field()
    detail = scrapy.Field()
    response = scrapy.Field()
    extra_info = scrapy.Field()
    created_at = scrapy.Field()
    updated_at = scrapy.Field()
