from datetime import datetime
from scrapy.http import Request
from frontera.utils.misc import get_crc32, chunks
from frontera.utils.fingerprint import sha1
from frontera.utils.url import parse_domain_from_url_fast
from time import time, sleep

def _create_page(url):
    _, hostname, _, _, _, _ = parse_domain_from_url_fast(url)
    fingerprint  = sha1(url)
    db_page = {}
    db_page['fingerprint'] = sha1(url)
    db_page['url'] = url
    db_page['created_at'] = time()*1E+6
    db_page['meta'] = {
        'scrapy_callback': None,
        'scrapy_meta': {},
        'scrapy_errback': None,
        'state': 1,
        'depth': 0,
        'fingerprint': fingerprint,
        'origin_is_frontier': True
    }
    db_page['host_crc32'] = get_crc32(hostname)
    db_page['depth'] = 0
    db_page['score'] = 0
    db_page['headers'] = {}
    db_page['method'] = 'GET'
    db_page['cookies'] = {}
    db_page['partition_id'] = 0

    return db_page
from pymongo import MongoClient

def seed(filePath):
    f = open(filePath)
    start_urls = [url.strip() for url in f.readlines()]
    f.close()
    client = MongoClient()
    db = client.mangafox
    for url in start_urls:
        if url.find('#') < 0:
            obj = _create_page(url)
            db.queue.update({'fingerprint': obj['fingerprint']}, {'$set': obj}, upsert=True)
    print 'done'
# seed("../../seed.txt")