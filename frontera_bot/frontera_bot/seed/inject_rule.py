data = [
    {
        'hostname' : 'mangastream.com',
        'rule':{
            'page1': {
                'identify' : {
                    'url_regex' : ['http://mangastream.com/manga/([^/])+'],
                },
                'fields' : {
                    'name':{
                        'xpath' : '/html/body/div[3]/div[2]/div[1]/h1/text()',
                        'type': 'string',
                    },
                    'list_chap': {
                        'xpath': '/html/body/div[3]/div[2]/div[1]/table/tr/td[1]/a/@href',
                        'type': 'array',
                    },
                },
                'extra_info':{
                    'store': 'story'
                }

            },

        }
    },
    {
        'hostname' : 'www.24h.com.vn',
        'rule':{
            'page1': {
                'identify' : {
                    'url_regex' : ['http://www.24h.com.vn/bong-da/([^/])+'],
                },
                'fields' : {
                    'title':{
                        'xpath' : '/html/body/div[2]/div[3]/table//tr[1]/td[2]/div/div[2]/div[1]/h1/text()',
                        'type': 'string',
                    },
                    'description': {
                        'xpath': '/html/body/div[2]/div[3]/table//tr[1]/td[2]/div/div[2]/div[1]/p/text()',
                        'type': 'string',
                    },
                },
                'extra_info': {
                    'store': '24h'
                }
            },

        }
    },
    {
        'hostname': 'mangafox.me',
        'rule': {
            'story': {
                'identify': {
                    'url_regex': ['^http://mangafox.me/manga/([^/])+/$'],
                },
                'fields': {
                    "name": {
                        'xpath': '//*[@id="title"]/h1/text()',
                        'type': 'string'
                    },
                    "avatar": {
                        'xpath': '//*[@id="series_info"]/div[1]/img/@src',
                        'type': 'string'
                    },
                    "other_name": {
                        'default' : '',
                        'xpath': '//*[@id="title"]/h3/text()',
                        'type': 'string'
                    },
                    "released": {
                        'default': '',
                        'xpath': '//*[@id="title"]/table//tr[2]/td[1]/a/text()',
                        'type': 'string'
                    },
                    "author": {
                        'default': '',
                        'xpath': '//*[@id="title"]/table//tr[2]/td[2]/a/text()',
                        'type': 'array'
                    },
                    "artist": {
                        'default': '',
                        'xpath': '//*[@id="title"]/table//tr[2]/td[3]/a/text()',
                        'type': 'array'
                    },
                    "genre": {
                        'xpath': '//*[@id="title"]/table/tr[2]/td[4]/a/text()',
                        'type': 'array'
                    },
                    "describe": {
                        'default': '',
                        'xpath': '//*[@id="title"]/p/text()',
                        'type': 'string'
                    },
                    "status": {
                        'default': 'Ongoing',
                        'xpath': '//*[@id="series_info"]/div[5]/span/text()',
                        'type': 'string',
                        'regex_extract' : ['(\w+)'],
                    },
                    "rank": {
                        'default': 0,
                        'xpath': '//*[@id="series_info"]/div[6]/span/text()',
                        'type': 'string',
                        'regex_extract' : ['(\d+)th'],
                    },
                    "view": {
                        'default': 0,
                        'xpath': '//*[@id="series_info"]/div[6]/span/text()',
                        'type': 'string',
                        'regex_extract': ['has ([0-9,]+) '],
                        'regex_sub': [
                            {
                                'pattern': ',',
                                'replace': ''
                            }
                        ]
                    },
                    "rating": {
                        'default' : 0.0,
                        'xpath': '//*[@id="series_info"]/div[7]/span/text()',
                        'type': 'string',
                        'regex_extract': ['Average ([\d.]+) '],
                    },
                },
                'extra_info': {
                    'store': 'story'
                }
            },
            'chapter': {
                'identify': {
                    'url_regex': ['http://mangafox.me/manga/([^/])+/(.)+'],
                },
                'fields': {
                    "name": {
                        'default' : '',
                        'xpath': '/html/body/table//tr[175]/td[2]/span/span[7]/text()',
                        'regex_extract' : ['(.*?):(.*)'],
                        'type': 'string'
                    },
                    "story_url": {
                        'xpath': '//*[@id="series"]/strong[3]/a/@href',
                        'type': 'string'
                    },
                    "content": {
                        'default' : '',
                        'xpath': '//*[@id="image"]/@src',
                        'type': 'array'
                    },
                    "page": {
                        'xpath': '//*[@id="series"]/strong[2]/text()',
                        'type': 'string',
                        'regex_extract':['([\d]+)'],
                    },
                    "totalpage": {
                        'xpath': '//*[@id="top_bar"]/div/div/text()[2]',
                        'type': 'string',
                        'regex_extract':['([\d]+)'],
                    }
                },
                'extra_info': {
                    'id' : 'schapter',
                    'plugin': 'mangafox_plugin'
                }
            },
        }
    },
    {
        'hostname': 'm.mangafox.me',
        'rule': {
            'm_chapter': {
                'identify': {
                    'url_regex': ['http://m.mangafox.me/roll_manga/([^/])+/(.)+'],
                },
                'fields': {
                    "story_url": {
                        'xpath': '/html/body/div[2]/header/a[1]/@href',
                        'type': 'string'
                    },
                    "content": {
                        'xpath': '//*[@id="viewer"]/img/@data-original',
                        'type': 'array'
                    },
                },
                'extra_info': {
                    'id' : 'mchapter',
                    'plugin': 'mangafox_plugin'
                }
            },
        }
    },
    {
        'hostname': 'coderdeptrai.com',
        'rule': {
            'm_chapter': {
                'identify': {
                    'url_regex': ['http://m.mangafox.me/roll_manga/([^/])+/(.)+'],
                },
                'fields': {
                    "story_url": {
                        'xpath': '/html/body/div[2]/header/a[1]/@href',
                        'type': 'string'
                    },
                    "content": {
                        'xpath': '//*[@id="viewer"]/img/@data-original',
                        'type': 'array'
                    },
                },
                'extra_info': {
                    'id': 'mchapter',
                    'plugin': 'mangafox_plugin'
                }
            },
        }
    },
        ]
from pymongo import MongoClient

client = MongoClient("mongodb://45.76.155.69:27017")
db = client.filter
db.filter.remove({})
db.filter.insert(data)
print 'done'
