from frontera_bot.utils.filter import MangaResponseFilter
from frontera.utils.url import parse_domain_from_url_fast
from scrapy.cmdline import execute
import urllib2
from scrapy.http import HtmlResponse
from scrapy.utils.project import get_project_settings
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

links = [
    # 'http://mangafox.me',
    'http://mangafox.me/manga/suki_x_suki_hibaru_shunsuke/'
]
# print execute(['scrapy','fetch','--spider=mangafox',links[0]])
settings = get_project_settings()
settings.set('MONGO_URI','localhost:27017')
settings.set('MONGO_DATABASE','filter')
# settings.set('LOG_LEVEL','DEBUG')
def parse(response):
    parser = MangaResponseFilter(settings)
    _, hostname, _, _, _, _ = parse_domain_from_url_fast(response.url)
    obj = parser.getRule(hostname)
    if obj is None:
        raise Exception('Not found rule by hostname: {0} '.format(hostname))
    fields, extra_info = parser.getFieldsParse(obj['rule'], response)
    if fields is None:
        return {}, False
    doc = parser.parse(fields, response)
    return doc, True
for url in links:
    print '************************ Parse link: ' + url + '*************************'
    response = urllib2.urlopen(url)
    webContent = response.read()
    response = HtmlResponse(url=url, body=webContent)
    doc, result = parse(response)
    if  result == True:
        for key, value in doc.items():
            print key + ' -> ' + str(value) + '\n'
    print '***********************************************************'